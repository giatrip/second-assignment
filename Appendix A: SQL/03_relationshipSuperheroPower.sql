DROP TABLE IF EXISTS superhero_superpower;
CREATE TABLE superhero_superpower(
	sup_id int REFERENCES superhero,
	pow_id int REFERENCES superpower,
	PRIMARY KEY (sup_id, pow_id)
);

