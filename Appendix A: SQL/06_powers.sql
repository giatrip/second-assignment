INSERT INTO superpower (pow_name) VALUES ('Flight');
INSERT INTO superpower (pow_name,pow_description) VALUES ('Strength Infinitum','Unlimited amount of strength');
INSERT INTO superpower (pow_name,pow_description) VALUES ('X_RAY Vision','Ability to see through obstacles');
INSERT INTO superpower (pow_name,pow_description) VALUES ('Engineer','Know-how to technology');

INSERT INTO superhero_superpower (sup_id,pow_id) VALUES (1,4);
INSERT INTO superhero_superpower (sup_id,pow_id) VALUES (2,1);
INSERT INTO superhero_superpower (sup_id,pow_id) VALUES (2,2);
INSERT INTO superhero_superpower (sup_id,pow_id) VALUES (2,3);
INSERT INTO superhero_superpower (sup_id,pow_id) VALUES (3,1);
INSERT INTO superhero_superpower (sup_id,pow_id) VALUES (3,4);
